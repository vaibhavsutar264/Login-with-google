const express = require("express")

const { signinController, signupController,getUserDetails,logout } = require("../controllers/userController")

const router = express.Router()

router.post("/signin", signinController)
router.post("/signup", signupController)
router.get("/get-user/:id", getUserDetails)
router.get("/logout", logout)

module.exports = router;