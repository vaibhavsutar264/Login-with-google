const express = require("express");
const { processPayment,getUserPaymentDetails} = require("../controllers/paymentController");
const {newResume,myResume} = require('../controllers/resumeController');
const { isAuthenticatedUser } = require("../middleware/auth");

const router = express.Router()

router.route("/add-resume").post(isAuthenticatedUser, newResume);
router.route("/get-resume/:id").get(isAuthenticatedUser, myResume);
router.post("/charge", processPayment)
router.post("/check-payment", getUserPaymentDetails)

module.exports = router;