const catchAyncErrors = require("./catchAsyncErrors");
const jwt = require("jsonwebtoken");
const User = require("../models/user");
exports.isAuthenticatedUser = catchAyncErrors(async(req,res,next)=>{
    const authHeader = req.headers.authorization;
    if (authHeader && authHeader.startsWith('Bearer ')) {
        const token = authHeader.split(' ')[1];
        if(!token){
            res.status(500).json({
                message:"Invalid token"
            })
        }
        const decodedData = jwt.verify(token, process.env.JWT_SECRET);
        req.user = await User.findById(decodedData.id); 
        next();
      }
});
