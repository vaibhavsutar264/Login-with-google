const mongoose = require("mongoose")

const userSchema = mongoose.Schema({
    username: {type: String},
    email: {type: String, required: true},
    contact_number: {type: Number},
    password: {type: String},
    payment_status: {type: String},
})

module.exports = mongoose.model("User", userSchema)