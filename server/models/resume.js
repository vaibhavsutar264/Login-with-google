const mongoose = require('mongoose');

const resumeSchema = new mongoose.Schema({
  basicInfo: {
    sectionTitle: {
      type: String,
    //   required: true
    },
    detail: {
      type: Object,
      default: {}
    }
  },
  workExp: {
    sectionTitle: {
      type: String,
    },
    details: {
      type: Array,
      default: []
    }
  },
  project: {
    sectionTitle: {
      type: String,
    //   required: true
    },
    details: {
      type: Array,
      default: []
    }
  },
  education: {
    sectionTitle: {
      type: String,
    //   required: true
    },
    details: {
      type: Array,
      default: []
    }
  },

  achievement: {
    sectionTitle: {
      type: String,
    //   required: true
    },
    points: {
      type: Array,
      default: []
    }
  },
  summary: {
    sectionTitle: {
      type: String,
    //   required: true
    },
    detail: {
      type: String,
      default: ''
    }
  },
  other: {
    sectionTitle: {
      type: String,
    //   required: true
    },
    detail: {
      type: String,
      default: ''
    }
  },
  user: {
    type: mongoose.Schema.ObjectId,
    ref:"User",
    required: true,
}
});
module.exports = mongoose.model('Resume', resumeSchema);
