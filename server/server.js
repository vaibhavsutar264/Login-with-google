const express = require("express")
const mongoose = require('mongoose')
const cors = require('cors')
const morgan = require('morgan')
const helmet = require("helmet");
const userRoutes = require("./routes/userRoutes")
const resumeRoute = require("./routes/resumeRoute")
const dotenv = require("dotenv")
const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;
const jwt = require('jsonwebtoken');
dotenv.config();
const app = express()
app.use(cors())
app.use(express.json())
app.use(helmet());
app.use(helmet.crossOriginResourcePolicy({ policy: "cross-origin" }));
app.use(morgan('dev'));
app.use("/users", userRoutes)
app.use("/resume", resumeRoute)

const PORT = process.env.PORT || 5000;
const MONGOOSE_URL = process.env.MONGO_URI

passport.use(
    new FacebookStrategy(
      {
        clientID: process.env.FACEBOOK_APP_ID,
        clientSecret: process.env.FACEBOOK_APP_SECRET,
        callbackURL: '/auth/facebook/callback',
      },
      (accessToken, refreshToken, profile, done) => {
        // Here, you can perform operations like saving the user profile to the database or creating a new user.
        // The 'profile' object contains the user's Facebook profile information.
        // You can access fields like profile.id, profile.displayName, profile.email, etc.
        console.log(profile);
        done(null, profile);
      }
    )
  );
  
  // Configure routes
  app.get('/auth/facebook', passport.authenticate('facebook'));
  
  app.get(
    '/auth/facebook/callback',
    passport.authenticate('facebook', { session: false }),
    (req, res) => {
      // Generate JWT token and send it in the response
      const payload = { id: req.user.id, name: req.user.displayName };
      const token = jwt.sign(payload, process.env.JWT_SECRET);
      res.json({ token });
    }
  );


mongoose.connect(MONGOOSE_URL, {useNewUrlParser: true})
.then(()=> app.listen(PORT, ()=>{
    console.log(`Server is running at port ${PORT} and connected to mongodb url as ${MONGOOSE_URL}`);
}))
.catch(err=>{
    console.log(err)
})