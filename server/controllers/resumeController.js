const catchAsyncErrors = require('../middleware/catchAsyncErrors');
const Resume = require('../models/resume')


const newResume = catchAsyncErrors(async(req,res,next)=>{
    const {
        achievement,
        basicInfo,
        education,
        project,
        summary,
        workExp,
        other,
        user,
    } = req.body;

    const resumeData = await Resume.create({
        achievement,
        basicInfo,
        education,
        project,
        summary,
        workExp,
        other,
        // user
        user:req.user._id,
    });

    res.status(201).json({
        success:true,
        resumeData,
    });
});


const myResume = catchAsyncErrors(async (req,res,next)=>{
    const myResumeDetails = await Resume.find({ user: req.user._id });

    res.status(200).json({
        success:true,
        myResumeDetails,
    })
})


module.exports = {
    newResume,
    myResume
}