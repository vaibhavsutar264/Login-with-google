const catchAsyncErrors = require("../middleware/catchAsyncErrors");
const User = require("../models/user")
// const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);
// const secret = process.env.STRIPE_SECRET_KEY
const stripe = require("stripe")('sk_test_51LVH7aSHR0RldS5Sns3v8VvpXDj2qLlZQtbPivDulE3pLEcxSj5Iqi2X2pgd4pVeMcoNzizcEDfoiNc976bAnk2A00M94bOLt7');
const processPayment = catchAsyncErrors(async (req, res, next) => {
  console.log(stripe);
  try {
  const session = await stripe.checkout.sessions.create({
      payment_method_types: ['card'],
      line_items: [
        {
          price_data: {
            currency: 'inr',
            product_data: {
              name: 'Paid Resume',
            },
            unit_amount: req.body.amount, // Amount in cents (100 Rs)
          },
          quantity: 1,
        },
      ],
      mode: 'payment',
      success_url: 'http://localhost:3000/resume?session_id={CHECKOUT_SESSION_ID}',
      cancel_url: 'http://localhost:3000/cancel',
    });
  
    res.status(200).json({ id: session.id });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});


const getUserPaymentDetails = catchAsyncErrors(async (req, res, next) => {
  try {
    const session = await stripe.checkout.sessions.retrieve(req.body.sessionId);
    const { payment_status } = session;
    if(payment_status == "paid"){
      const newUserData = {
        payment_status : "paid"
      }
      const user = await User.findByIdAndUpdate(req.body.userId, newUserData, {
        new: true,
        runValidators: true,
        useFindAndModify:false,
    })
    res.status(200).json({ user });
    }
    
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

module.exports = {
  processPayment,
  getUserPaymentDetails
}

