const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")
const axios = require("axios")
const User = require("../models/user")
const catchAsyncErrors = require("../middleware/catchAsyncErrors")


const signinController = catchAsyncErrors(async(req, res) => {
    if(req.body.googleAccessToken){
        // gogole-auth
        const {googleAccessToken} = req.body;

        axios
            .get("https://www.googleapis.com/oauth2/v3/userinfo", {
            headers: {
                "Authorization": `Bearer ${googleAccessToken}`
            }
        })
            .then(async response => {

                const email = response.data.email;

                const existingUser = await User.findOne({email})

                if (!existingUser) 
                return res.status(404).json({message: "User don't exist!"})

                const token = jwt.sign({
                    email: existingUser.email,
                    id: existingUser._id
                }, process.env.JWT_SECRET, {expiresIn: "1h"})
        
                res
                    .status(200)
                    .json({result: existingUser, token})
                    
            })
            .catch(err => {
                res
                    .status(400)
                    .json({message: "Invalid access token!"})
            })
    }else{
        // normal-auth
        const {email, password} = req.body;
        if (email === "" || password === "") 
            return res.status(400).json({message: "Invalid field!"});
        try {
            const existingUser = await User.findOne({
                $or: [{ username: email }, { email: email }],
              })
    
            if (!existingUser) 
                return res.status(404).json({message: "User don't exist!"})
    
            const isPasswordOk = await bcrypt.compare(password, existingUser.password);
    
            if (!isPasswordOk) 
                return res.status(400).json({message: "Invalid credintials!"})
    
            const token = jwt.sign({
                email: existingUser.email,
                id: existingUser._id
            }, process.env.JWT_SECRET, {expiresIn: "2h"})
    
            res
                .status(200)
                .json({result: existingUser, token})
        } catch (err) {
            res
                .status(500)
                .json({message: "Something went wrong!"})
        }
    }
  
})

const signupController = catchAsyncErrors(async(req, res,next) => {
    if (req.body.googleAccessToken) {
        const {googleAccessToken} = req.body;
        axios
            .get("https://www.googleapis.com/oauth2/v3/userinfo", {
            headers: {
                "Authorization": `Bearer ${googleAccessToken}`
            }
        })
            .then(async response => {
                const email = response.data.email;

                const existingUser = await User.findOne({email})

                if (existingUser) 
                    return res.status(400).json({message: "User already exist!"})

                const result = await User.create({email})

                const token = jwt.sign({
                    email: result.email,
                    id: result._id
                }, process.env.JWT_SECRET, {expiresIn: "1h"})

                res
                    .status(200)
                    .json({result, token})
            })
            .catch(err => {
                res
                    .status(400)
                    .json({message: "dcdc"})
            })

    } else {
        // normal form signup
        const {email, password, username, contact_number} = req.body;

        try {
            if (email === "" || password === "" || username === "" || contact_number === "" ) 
                return res.status(400).json({message: "Invalid field!"})

            const existingUser = await User.findOne({email})

            if (existingUser) 
                return res.status(400).json({message: "User already exist!"})

            const hashedPassword = await bcrypt.hash(password, 12)

            const result = await User.create({username,email,contact_number, password: hashedPassword})

            const token = jwt.sign({
                email: result.email,
                id: result._id
            }, process.env.JWT_SECRET, {expiresIn: "1h"})

            res
                .status(200)
                .json({result, token})
        } catch (err) {
            res
                .status(500)
                .json({message: "Something went wrong!"})
        }

    }
})


const getUserDetails = catchAsyncErrors(async(req,res,next)=>{
    try {
      const user = await User.findById(req.params.id);
      res.status(200).json({
        success: true,
        user,
      });
    } catch (error) {
      res.status(500).json({
        success: false,
        message: "There is no such user present",
      });
    }
    
});
const logout = catchAsyncErrors(async(req,res,next)=>{
      res.status(200).json({
        success: true,
        message:"Log Out Successful"
      });
    
});

module.exports = {
    signinController,
    signupController,
    getUserDetails,
    logout
}